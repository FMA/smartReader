# Smart Reader - Bionic Reader
Fork of Smart Reader to remove limit of minimum character. 

Smart reader is a way to keep our focus on the web.
The fact that the beginnings of words are bolded, keeps your eyes on alert (like bionic reading).

# Installation of Smart Reader original
## Firefox (Manifest v2)
### https://addons.mozilla.org/fr/firefox/addon/smartreader/

## Chrome version (Manifest v3)
### https://chrome.google.com/webstore/detail/smart-reader-bionic-readi/cgkfhliknoajpncfpnbmknalklgiekdj

## Contact to original
mailto:contact@smartreader.ovh
